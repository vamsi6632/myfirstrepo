/** This is my first Technical Documentation Project
 * 
 * Author: Nishanth
  */

 #include <iostream>

 /// this is my base class.
 // all other classes should be inherited from the base class.
 class Base
 {
     public:

     ///this function sum(int n) takes an integer n
     /// and returns the sum from 1 ... to n
     int sum(int n)
     {
         if(n < 2)
            return n;
        else
            return n + sum(n-1);
     }
 };

 /// this is derived class from class base
 /// this is solely for demonstration purpose
 class Derived: public Base
 {
     public:

     /// this pro(int n) takes an integer and returns
     /// the product of the integers from 1 to n.
     int pro(int n)
     {
         if(n < 2)
            return n;
        else
            return n * pro(n-1);
     }
 };

 /// this is the startup routine
 int main()
 {

     std::cout << "sum from 1 to 10 : "
        << Base{}.sum(10) <<std::endl;
        
        std::cout << "Products from 1 to 10 :"
        << Derived{}.pro(10) << std::endl;
 }