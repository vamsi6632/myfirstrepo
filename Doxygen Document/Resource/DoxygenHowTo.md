# Technical  Doxygen Documentation

In this, I have Created my first Doxygen Documentation of C++ program for Calculation of the surface Area and volume of cone

## c++ Checking surface area and volume Example

```c
/// this is a typical example 
/// of checking surface and volume using functions.

#include<iostream>

#include<cmath>

using namespace std;

int main()

{

    double r,h,a;

    cout<<"enter the radius="<<endl;

    cin>>r;

     cout<<"enter the hight="<<endl;

    cin>>h;

    if (r==0 || h==0)

    {

             cout<<"invalid operation"<<endl;
             

             }

    if(r!=0 && h!=0)

    {

            a=(3.14*r*r*h)/3;

            cout<<"the area="<<a<<endl;

            }    

            system("pause");

            return 0;
                   
    }

## Output

Write a program that calculates the volume of a solid cone.The formula is: volume=(PI x radius² x height)/3

Read in the values for the parameters height and radius(as floating point numbers).

Then the program should calculate the volume considering the following restriction :radius and height must be greater than0

If the user types in a correct value for one of the variables ,the program should print out an error message and quit.Otherwise,it displays the result.

You can lear more about the advanced c++ programming technique for website (http://www.codebind.com/cpp/cpp-programs-examples/).

